from platform import system
import queue

from tkinter import Tk

from view.main_window import MainWindow
from producer import ProducerThread


class Application:
    def __init__(self):
        self.root = None
        self.producer = None
        self.producer_queue = queue.Queue()
        self.window = None
        self.console = None

    def run_app(self):
        self.console = None

        print("Starting app")
        self.root = Tk()
        self.root.title("Asynchronous Training APP")

        print("Starting Producer Thread")
        self.producer = ProducerThread(self.root, self.producer_queue)

        print("Crating a window...")
        self.window = MainWindow(self.root)

        print("Showing the window...")

        self.periodic_call()
        self.root.mainloop()

    def periodic_call(self):
        """
        Check every 200ms if there  is something new in the  queue
        :return:
        """
        while self.producer_queue.qsize():
            try:
                msg = self.producer_queue.get(0)
                self.window.log.Add(msg)
            except queue.Empty:
                pass
        self.root.after(200, self.periodic_call)


if __name__ == '__main__':
    app = Application()
    app.run_app()
