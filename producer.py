import time
import threading
import random


class ProducerThread:
    def __init__(self, master, queue):
        self.master = master

        # Create the queue
        self.producer_queue = queue

        # Set up the thread to asynchronous I/O
        # More  thread  can also be created nad used, if necessary
        self.running = 1
        self.thread1 = threading.Thread(target=self.worker_thread1)
        self.thread1.start()

    def worker_thread1(self):
        while self.running:
            time.sleep(rand.random() * 1.5)
            msg = rand.random()
            self.producer_queue.put(msg)


rand = random.Random()
