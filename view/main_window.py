from tkinter import Frame

from view.ui.interface_view import InterfaceView
from view.ui.scroll_log import ScrollLog


class MainWindow:
    def __init__(self, master):
        self.master_gui = master

        self.interfaces_frame = Frame(self.master_gui)
        self.spi_interface = InterfaceView(master=self.interfaces_frame, name="SPI")
        self.i2c_interface = InterfaceView(master=self.interfaces_frame, name="I2C")

        self.action_frame = Frame(self.master_gui)

        self.logs_frame = Frame(self.master_gui)
        self.log = ScrollLog(master=self.logs_frame, width=50)

        self.arrange_widgets()

    def arrange_widgets(self):
        self.interfaces_frame.grid(row=0, column=0)
        self.action_frame.grid(row=0, column=1)
        self.logs_frame.grid(row=1, columnspan=2)

        self.arrange_interfaces()

    def arrange_interfaces(self):
        self.spi_interface.grid(row=0, column=0)
        self.i2c_interface.grid(row=1, column=0)
