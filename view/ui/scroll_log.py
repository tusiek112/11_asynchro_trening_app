
import tkinter as tk


class ScrollLog:

    def __init__(self, master, width,):
        self._logwndsize = 8
        self._loglen = 250

        self.Log = tk.Listbox(master, height=self._logwndsize, width=width)
        self.Scroll = tk.Scrollbar(master, orient=tk.VERTICAL)
        self.Log.config(yscrollcommand=self.Scroll.set)
        self.Scroll.config(command=self.Log.yview)
        self.LogLen = self._loglen

        self.Log.grid(row=0, column=0)
        self.Log.configure(font=("Lucida Console", 8))
        self.Scroll.grid(row=0, column=2)

    def Clear(self):
        self.Log.delete(0, self.Log.size())
        self.Log.see(0)

    def Add(self, xstr):
        if self.Log.size() == self.LogLen:
            self.Log.delete(0)
        self.Log.insert(tk.END, xstr)
        self.Log.see(self.Log.size())


